import requests
import csv
from typing import List, Dict
from functools import reduce
import argparse


def fetch_data(bearer_token: str, url: str) -> dict:
    headers = {
        "Authorization": f"Bearer {bearer_token}"
    }
    data = []

    while url:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        json_response = response.json()
        data.extend(json_response.get('results', []))
        url = json_response.get('next')
    return data


def extract_answers(schema: dict) -> List[dict]:
    def reducer(entries: Dict[str, dict], result: dict) -> Dict[str, dict]:
        for position in result.get('positions', []):
            ticket_id = position['id']
            if not entries.get(ticket_id):
                entries[ticket_id] = {
                    'name': result.get('invoice_address', {}).get('name', ''),
                    'email': result.get('email', ''),
                    'ticketId': ticket_id,
                    'matrix': '',
                    'fas': ''
                }
            for answer in position.get('answers', []):
                if answer['question_identifier'] in {'matrix', 'fas'}:
                    entries[ticket_id][answer['question_identifier']] = answer['answer']  # noqa: E501
        return entries

    reduced_results = reduce(reducer, schema, {})
    return list(reduced_results.values())


def write_to_csv(entries: List[dict], file_name: str, display: bool = False) -> None:  # noqa: E501
    fieldnames = entries[0].keys()
    with open(file_name, mode='w+', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for entry in entries:
            writer.writerow(entry)

        if not display:
            return

        csv_file.flush()
        csv_file.seek(0)
        print(csv_file.read())


def main(token, organizer, event):
    event_url = f"https://pretix.eu/api/v1/organizers/{organizer}/events/{event}/orders/"  # noqa: E501
    data = fetch_data(token, event_url)
    entries = extract_answers(data)
    print(len(data))
    write_to_csv(entries, "matrix_answers.csv", display=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process event orders.')
    parser.add_argument('--token', type=str, required=True, help='API token')
    parser.add_argument('--organizer', type=str, required=True,
                        help='Event organizer')
    parser.add_argument('--event', type=str, required=True, help='Event name')

    args = parser.parse_args()

    main(args.token, args.organizer, args.event)
