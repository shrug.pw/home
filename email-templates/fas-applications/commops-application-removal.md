FAS CommOps application removal
===============================

This text is a basic template to use for removed applications to the CommOps FAS group. This can be sent to accompany the automatic removal email sent by FAS, to help give a better explanation for why their application was removed.

* **Subject**: $FAS_USERNAME: Your CommOps FAS application status

- - - - -

```
Hello $FAS_USERNAME,

You are receiving this email due to your pending status in the Community Operations (commops) group in the Fedora Account System (FAS).

To become an approved member of the FAS group, there are a few steps to follow first. You can see an outline of some of these steps on the wiki page for how to join the CommOps team.

    https://fedoraproject.org/wiki/CommOps/Join

If you haven't already, you can start by sending an introduction to the CommOps mailing list. In the intro, you can tell us a bit about who you are, any relevant background / skills you want to mention, and how we can help you get started. You can find a link to the mailing list below.

    https://lists.fedoraproject.org/admin/lists/commops@lists.fedoraproject.org/

In the meanwhile, you can look through some of the open tickets for some of the tasks that CommOps is working on. All of our tickets are found on our Pagure project page. You can log into Pagure with your FAS account. You can find a link to the Pagure page here:

    https://pagure.io/fedora-commops/issues

Does anything look interesting to you? Feel free to comment on any ticket with a question or ask if there's a way you can help. We can always use help and we are more than willing to help you get started on something. There's no such thing as a silly or dumb question!

If you're still interested in joining the Fedora CommOps team, take a look at some of the steps described on the wiki page. Take a look through some of the things we're working on and introduce yourself on the mailing list, if you haven't already. If you have a chance, try to stop by one of our meetings and say hello to the team! You can see when we meet in the Fedora calendar:

    https://apps.fedoraproject.org/calendar/meeting/4409/

You're also welcome to stop by our IRC channel on Libera.chat to say hello. Our channel is #fedora-commops on https://libera.chat/. If you need help with IRC or are unfamiliar with it, you can see this beginner's guide to IRC on the Fedora Magazine.

    https://fedoramagazine.org/beginners-guide-irc/

If you have any questions, feel free to ask on our mailing list, or you can directly mail me as well. Thanks for your interest, and hope to hear from you soon!
```
