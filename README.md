Community + Operations = **CommOps**
====================================

<!--
    Style rule: one sentence per line please!
    This makes git diffs easier to read. :)
-->

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/ "Fedora Docs content licensed under CC BY-SA 4.0")
[![Join us at #fedora-commops on Libera.chat](https://img.shields.io/badge/chat-on%20freenode-brightgreen.svg)](https://web.libera.chat "Join us at #fedora-commops on libera.chat")

Providing tools, resources, and utilities for different sub-projects of Fedora to improve effective communication


## What CommOps does

The following areas are examples of where CommOps focuses:

* Work closely with [Fedora Community Action and Impact Coordinator](https://docs.fedoraproject.org/fedora-project/council/fcaic.html) to prioritize key focus areas
* Assist [Fedora Program Manager](https://docs.fedoraproject.org/fedora-project/council/fpgm.html) with release preparations
* Support preparation and execution of Fedora Elections
* Work with sub-projects and teams to improve on-boarding methods and practices
* Use metrics and data to improve understanding of Fedora community
    * Support development of metrics tooling
* And more

Read more about us on [docs.fedoraproject.org](https://docs.fedoraproject.org/en-US/commops/ "Fedora Community Operations [CommOps] :: Fedora Docs").


## What can I do here?

[File a ticket](https://pagure.io/fedora-commops/new_issue) for topics such as the following:

* Culture
* Elections
* Storytelling
* Metrics
* Supporting sub-projects

You can also help us!
Read our [getting started guide](https://docs.fedoraproject.org/en-US/commops/contribute/join/) for new contributors to get involved.
After reviewing the guide, check out any open tickets marked as [**good first issue**](https://pagure.io/fedora-commops/issues?status=Open&tags=good+first+issue).
If you are not sure where to start, ask any questions or doubts on the [Discourse forum](https://discussion.fedoraproject.org/c/commops).

If you think an issue needs more urgent attention, contact the ticket assignee and see if they are working on it.
If you don't hear from them within a week, you can take ownership of the task.
Because this team is staffed mostly by volunteers, we may not notice new issues immediately when they are filed.


## Where to find us

Get in touch with us!
You can find us in [**these places**](https://docs.fedoraproject.org/en-US/commops/#find-commops).


## How to contribute to docs

See [CONTRIBUTING.md](https://pagure.io/fedora-commops/blob/master/f/.pagure/CONTRIBUTING.md).
