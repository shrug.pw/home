* Design Assets
** xref:cheat-cubes.adoc[Cheat Cubes]
** xref:joining-slide-deck.adoc[Fedora Slide Template]
** xref:joining-printouts.adoc[How to Join Fedora Resources]
** xref:org-charts.adoc[Organizational Charts]
** xref:mindshare-infographics.adoc[Mindshare Infographics]
** xref:team-logos.adoc[Team Logos]
