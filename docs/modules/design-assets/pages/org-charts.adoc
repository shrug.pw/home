include::ROOT:partial$attributes.adoc[]

= Fedora Project Organizational Chart

Find the Fedora Project Organization Chart source files https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Organizational%20Charts%20_%20Infographics/Fedora%20Project%20Organizational%20Chart[here].

image::https://docs.fedoraproject.org/en-US/project/_images/orgchart.png[Fedora Organizational Chart,100%]
