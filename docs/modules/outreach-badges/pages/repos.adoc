include::ROOT:partial$attributes.adoc[]

= Repo Badges
Dhairya Chaudhary; Justin W. Flory; Sumantro Mukherjee
:page-authors: {author}, {author_2}, {author_3}

== CommOps Busy Bee

The link:https://pagure.io/fedora-badges/issue/831[CommOps Busy Bee] badges is awarded for opening tickets on the link:https://pagure.io/fedora-commops/issues[CommOps pagure repo].

image::https://pagure.io/fedora-badges/issue/raw/files/4216ba774fac615a23106a47fc36d07b06770e7b41f4a00d4419de5db2049331-busybee1.png[alt="Busy Bee"]

== The Bees Knees

The link:https://pagure.io/fedora-badges/issue/832[Bees Knees] badge is award for closing tickets on the
link:https://pagure.io/fedora-commops/issues[CommOps pagure repo].

image::https://pagure.io/fedora-badges/issue/raw/files/4d7d22cbf3309681126596594103b56e894eaaee5e30e3160a31e02881d4fd23-beesknees1.png[alt="Busy Knees"]

== Onboarding Ideas Specialist

The link:https://pagure.io/fedora-badges/issue/836[Onboarding Ideas Specialist] badge is awarded for opening tickets on the
link:https://pagure.io/fedora-join/Fedora-Join/issues[Fedora Join SIG pagure repo].

image::https://pagure.io/fedora-badges/issue/raw/files/a94c54ba9dca17be06fc1ba3f52577bb87f84e71b439c6b50da1fec45c346e26-onboarding-idea1.png[alt="Onboarding Ideas"]


== Onboarding Specialist

The link:https://pagure.io/fedora-badges/issue/837[Onboarding Specialist] badge is awarded for closing tickets on the
link:https://pagure.io/fedora-join/Fedora-Join/issues[Fedora Join SIG pagure repo].

image::https://pagure.io/fedora-badges/issue/raw/files/b78a855c25d77f428d3325cbc4a314336b412600790f81eb6911767dd909d2e0-onboarding1.png[alt="Onboarding Specialists"]

== Welcoming Committee

The link:https://pagure.io/fedora-badges/issue/834[Welcoming Committee] badge is awarded for closing tickets on the
link:https://pagure.io/fedora-join/Welcome-to-Fedora/issues[Welcome-to-Fedora pagure repo]

image::https://pagure.io/fedora-badges/issue/raw/files/0d94003ab209b9a94621f8f8b9cf16386849b9e9b120a0dc56adc6b723bd299a-welcome1.png[alt="Welcoming Committee"]
