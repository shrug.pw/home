include::ROOT:partial$attributes.adoc[]

= Event Badges
Dhairya Chaudhary; Justin W. Flory; Sumantro Mukherjee
:page-authors: {author}, {author_2}, {author_3}

The following badges are awarded for participation in Fedora events.

== Advocates

The link:https://badges.fedoraproject.org/badge/fedora-advocate[Fedora Advocate badge] is awarded for being a organizing an event through the Advocate Program.

image::https://pagure.io/fedora-badges/issue/raw/files/192ea0085e39847f517c417ff42599256ac425324466cbcc5ad3e3384ae1a71d-633-advocate-2.png[alt="Advocates",align="center"]

== Panda Sighting

The link:https://pagure.io/fedora-badges/issue/835[Panda Sighting] badge is awarded for making an appearance on the Fedora YouTube Channel.

image::https://pagure.io/fedora-badges/issue/raw/files/7ce2c7cbdc27db57e7add0c8fd33790f546d2f847e8150a1b5fea70827bc6641-sighting.png[alt="Panda Sighting",align="center"]
