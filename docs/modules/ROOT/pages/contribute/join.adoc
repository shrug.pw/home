include::ROOT:partial$attributes.adoc[]

= Join Community Operations

Become a member of Fedora Community Operations (CommOps) by following these steps.


[[fas]]
== Create a FAS account

Create a {FWIKI}/Account_System[Fedora Account System account] and sign the {FWIKI}/Legal:Fedora_Project_Contributor_Agreement[Fedora Project Contributor Agreement].
Do this through the https://admin.fedoraproject.org/accounts/[Fedora Account System].


[[discourse]]
== Log into Fedora Discourse

Log into the {DISCOURSE}[Fedora Discourse forum] using your FAS account credentials.
News, updates, and discussion are shared through on the forum.
It is a *key part* to how we communicate.
It receives low to medium traffic.


[[introduction]]
== Post a self-introduction

Say hello and introduce yourself to the team!
{DISCOURSE}[Post a self-introduction] to the Discourse forum and tell us a little about yourself.

Not sure what to say?
Answer these questions to start:

* Why are you interested in contributing to Fedora?
* Why are you interested in contributing to CommOps?
* If you're involved with other things in Fedora, what are/were you working on?
* Do you have any experience in open source or online communities? If so, what?
* What parts of CommOps were interesting to you?
* Do you have any questions for us? How can we help _you_ get started?


[[community-blog]]
== Register on Community Blog

The Fedora {COMMBLOG}[Community Blog] is a key component to CommOps.
An account gives permissions to view article drafts and previews before they are published.
If you ever write an article, you will need an account.
To register, log in at the {COMMBLOG}/wp-login.php[login page] with your Fedora Account System (FAS) credentials.

Need more help logging in?
Read the {COMMBLOG}/how-to-log-in/[help page] for more detailed instructions.


[[irc]]
== Join our IRC channel

Fedora uses the https://libera.chat/[Libera.chat IRC network] for instant messaging and communication.
Short discussions and planning happen in our IRC channel #fedora-commops.
Our monthly team meetings also take place on IRC.

Never used IRC before?
See the https://fedoramagazine.org/beginners-guide-irc/[Beginner's Guide to IRC] on the Fedora Magazine.
Additionally, you can connect temporarily in a chat session in your browser via https://web.libera.chat/[Libera.chat web chat].
However, if you want to stay connected even when you are not online, consider https://opensource.com/article/17/5/introducing-riot-IRC[using Riot] as your IRC client.

For convenience, the IRC channel is bridged to a Telegram group, https://t.me/fedoracommops[@fedoracommops].
You may also participate via the Telegram group, but you are unable to use or interact with IRC bots (e.g. in meetings).
Join the group at https://t.me/fedoracommops[t.me/fedoracommops].


[[first-steps]]
== Make your first steps

See xref:contribute/first-steps.adoc[Make your first steps in Fedora] for more info.


[[start-task]]
== Start on a task

CommOps uses https://pagure.io/fedora-commops[this Pagure repository] to track our tasks and what we're working on.
Tasks, problems, and goals are filed as tickets in the https://pagure.io/fedora-commops/issues[issue tracker].
There are a lot of tickets, but we're not working on everything at once.
Scrolling through the issue tracker gives a good idea of what CommOps helps with.

Look for the https://pagure.io/fedora-commops/issues?status=Open&tags=good+first+issue[good first issue] tag on tickets to find good tasks for beginners.

Does something look interesting?
Leave a comment if you have a question or want to work on something.
Also check and see if the ticket is assigned to someone.
If it is unassigned, you can start working on it now.
If there is an assignee, ask them if they need help or if they are still working on the ticket.

This provides an opportunity to receive mentorship and get help to start a new task.


[[meetings]]
== Join our meetings

CommOps has an IRC team meeting each week.
In our meeting, the team discusses items on our agenda and checks in on any progress of current tasks.
Tickets selected for meetings are updated after the meeting with new information.

The meetings are open to the public and anyone can join.
See when our meetings are in your time zone on the https://apps.fedoraproject.org/calendar/commops/[CommOps meeting calendar].
Feel free to attend and introduce yourself during roll call!
