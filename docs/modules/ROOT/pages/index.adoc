= CommOps Teams
:page-layout: with_menu

CommOps Teams are focused on helping our community with onboarding, outreach and metrics

* https://docs.fedoraproject.org/en-US/commops/teams/ambassadors/[**Ambassadors**] - Ambassadors are representatives of Fedora and people who organize medium and large events

* https://docs.fedoraproject.org/en-US/mindshare-committee/advocate/[**Advocates**] - Advocates are people who organize or help organize small events

* xref:contribute/commops-landing.adoc[**CommOps**] - Community Operations (CommOps) provides tools, resources, and utilities for different sub-projects of Fedora to improve effective communication

* https://docs.fedoraproject.org/en-US/fedora-join/[**Join**] - The Fedora Join Special Interest Group (SIG) aims to set up and maintain channels that let prospective contributors engage with the community

* https://docs.fedoraproject.org/en-US/marketing/[**Marketing**] - The Fedora Marketing Team ensures that people in Fedora can consistently explain to everyone what Fedora is, why the project can help them, and how they can help the project.

