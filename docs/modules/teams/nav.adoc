* Teams
** Ambassadors
*** xref:ambassadors.adoc[Team Overview]
*** xref:ambassador-role.adoc[Role Handbook]
*** xref:ambassador-logo.adoc[Logos]
*** xref:ambassador-badges.adoc[Badges]
*** xref:become-an-ambassador.adoc[Become an Ambassador]
*** xref:ambassador-emeritus.adoc[Ambassador Emeritus]
**** xref:ambassador-emeritus-role.adoc[Role Handbook]
**** xref:ambassador-emeritus-badges.adoc[Badges]
**** xref:ambassador-emeritus-logo.adoc[Logos]
*** xref:ambassador-mentor.adoc[Ambassador Mentor]
**** xref:ambassador-mentor-role.adoc[Role Handbook]
** CommOps
*** xref:commops.adoc[Team Overview]
*** xref:commops-role.adoc[Member Roles]
*** xref:commlogo.adoc[Logos]
*** xref:commbadges.adoc[Badges]
*** xref:community-blog.adoc[Community Blog]
*** Community Polls
**** xref:community-polls.adoc[Overview]
**** xref:question-format.adoc[Template]
**** xref:most-asked.adoc[Most Asked Questions]
*** How to contribute
**** xref:contribute/join.adoc[Join CommOps]
** link:https://docs.fedoraproject.org/en-US/mindshare-committee/advocate/[Advocates pass:[<i class="fas fa-external-link-alt"></i>]]
** link:https://docs.fedoraproject.org/en-US/fedora-join/[Join SIG pass:[<i class="fas fa-external-link-alt"></i>]]
** link:https://docs.fedoraproject.org/en-US/marketing/[Marketing pass:[<i class="fas fa-external-link-alt"></i>]]
** xref:mindshare-reps.adoc[Reps to Mindshare]
** Misc Documentation
*** xref:irc-group-contact.adoc[IRC/Libera Group Contacts]
