include::ROOT:partial$attributes.adoc[]

= Questions

Below are the set of question which can serve as example.
-------------------------------------------------------------------------
What Linux/FOSS related groups are you connected with in your local area?

[poll name=poll2 type=multiple results=always min=1 max=6 chartType=bar]

    Government
    Hobbiest
    Open Source Communities
    Professional
    Tech Communities
    University/College/Grade School
    None
    Other
    [/poll]

If you fall into the “Other” category, please tell us more about that in the comments section. Thank you!
-------------------------------------------------------------------------

-----------------------------------------------------------------------------------
What kind of participation have you had in Fedora’s Outreach in the past/currently?

[poll name=poll2 type=multiple results=always min=1 max=7 chartType=bar]

    Attendance/representation at events
    Casual/Personal Networks
    Coordinating/organizing events
    Creating/organizing demos/Workshops
    Promotion/outreach at schools or universities
    Outreach in professional networks
    Online Promotion
    None
    Other
    [/poll]

If you fall into the “Other” category, please tell us more about that in the comments section. Thank you!
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
How many non-Fedora organized events(in person or virtual) did you attend this year and talk about the Fedora Project?

[poll type=regular results=always chartType=bar]

    None
    1-5 events
    More than 5
    More than 10
    More than 20
    [/poll]
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Who would you first try to contact if you wanted to run a Fedora event/needed swag?


[poll type=regular results=always chartType=bar]

    FCAIC
    Fedora Council
    Fedora Documentation
    IRC/Element/Telegram
    Local User Group
    Mindshare Committee
    Online Forum(Ask, Discussion)
    Regional Fedora Ambassador group
    I don’t know
    Not applicable
    Other
    [/poll]

If you fall into the “Other” category, please tell us more about that in the comments section.
Thank you!

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Did you attend Nest with Fedora?

[poll type=regular results=always chartType=bar]

    Yes, 2020
    Yes, 2021
    Yes, 2020 & 2021
    No, I couldn’t make it to either.
    What is Nest with Fedora?
    [/poll]
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
On a scale from 1-5 how familiar are you on how Fedora is organized as a project?

[poll type=regular results=always chartType=bar]

    1 (unfamiliar)
    2
    3
    4
    5 (familiar)
    [/poll]
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
On a scale of 1-5, how accessible do you find Fedora resources to be? (such as docs, wiki pages, pagure repo, IRC/chatroom)

[poll type=regular results=always chartType=bar]

    1 (not accessible)
    2
    3
    4
    5 (easily accessible)
    [/poll]
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Are you aware that Fedora provides concrete resources to further Fedora’s vision and initiatives? (event sponsorships, travel assistance, swag for local projects/events)

[poll type=regular results=always chartType=bar]

    Yes, and I have taken advantage of those resources
    Yes, but I have no need to request them
    Yes, but the process did not go well for me
    Yes, but I am not sure how to request resources
    No, I was previously unaware
    Other
    [/poll]
    
If you fall into the “Other” category, please tell us more about that in the comments section. Thank you!
-----------------------------------------------------------------------------------
-------------------------------------------------------------------------------
How satisfied are you with community outreach within the Fedora community?

[poll type=regular results=always chartType=bar]

    1 (not satisfied)
    2
    3
    4
    5 (completely satisfied)
    [/poll]
    
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
How satisfied are you with community outreach outside of the Fedora community?

[poll type=regular results=always chartType=bar]

    1 (not satisfied)
    2
    3
    4
    5 (completely satisfied)
    [/poll]
-------------------------------------------------------------------------------    
-------------------------------------------------------------------------------
Do you feel like you know what is going in the Fedora community?

[poll type=regular results=always chartType=bar]

    1 (no idea)
    2
    3
    4
    5 (I feel well informed)
    [/poll]
    
-------------------------------------------------------------------------------
What would you be doing even more of in Fedora if you had the resources?

    Open ended?
    
---------------------------------------------------------------------------------
