include::ROOT:partial$attributes.adoc[]

= Ambassador Emeritus Member

Life happens, things get busy, and interests change. This is a natural part of life that we embrace in the Fedora community. Whether you are moving to a new team in Fedora or taking a break altogether, it is important to have an avenue to retire. https://accounts.fedoraproject.org/group/ambassadors/[Ambassador team members] looking to retire should send an email to the https://lists.fedoraproject.org/admin/lists/ambassadors.lists.fedoraproject.org/[Ambassador Mailing List] with a retirement message- it doesn’t have to be long (or it can be!) to let your teammates know you will be retiring or taking a break. This gives everyone a chance to thank you for all your efforts in the Ambassador Program!

The CommOps Team will also run a yearly group cleanup. This entails running a script to search for inactive users(Fedora Account System) and reaching out to them directly via their Fedora Project email. It is absolutely fine to respond to this as “I want to stay involved” or “I will be back soon” or “I am unable to be a part at this time”.

== How does someone come back?


Just like the first time you joined Fedora, you are always welcomed to come back and join. Same as the first time you joined the Ambassadors group, you should create a thread at the https://discussion.fedoraproject.org/tag/ambassadors[Ambassadors Disc.] in order to express your interest to come back, give a brief explanation of the reason that made you retire (no personal details are required!) and share some ideas about how you can help the community. If you have been away for a long time and you would like some “Fedora orientation”, don’t forget to mention that as well.

In the meantime, you can check what the rest of the community is up to by reading the Mailing Lists’ archives, checking Fedora social media accounts, check Fedora Discussion, have a look at the Fedora repos in order to see what the community is currently working on. Don’t hesitate to participate in all the above by providing your assistance when help is needed or by simply joining that discussion.
