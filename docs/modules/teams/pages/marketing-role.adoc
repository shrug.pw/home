include::ROOT:partial$attributes.adoc[]

= Fedora Marketing Team Member
The Fedora Marketing Team develops and executes marketing strategies to promote the usage and support of Fedora worldwide. Through the development of processes and content, this project aims to support the efforts of other Fedora projects to spread Fedora and to provide a central source of ideas and information that can be used to deliver Fedora to new audiences. The marketing Team works closely with the https://docs.fedoraproject.org/en-US/fedora-join/[Join SIG] and https://docs.fedoraproject.org/en-US/commops/teams/ambassadors/[Fedora Ambassadors] who spread the word about Fedora at events and allow the Fedora Project to interact directly with its existing and prospective users.

`More content needs to be added below.`

### How to join the team/ step down


### Responsibilities 

  

### Teams you will be closely working with

  

### Current member

  

### Contact information 

  

### Information Sheet
