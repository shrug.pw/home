include::ROOT:partial$attributes.adoc[]

= IRC / Libera Chat Group Contacts

https://libera.chat/[libera.chat] IRC network has a concept of ‘Groups’ for various projects. Group contacts for each of those groups are granted special powers on the network over their groups channels. Additionally, requests from Group contacts can be taken as ‘official’ requests from the group. Fedora group contacts represent Fedora to libera.chat.

The primary functions of group contacts are:

* Check/process ‘cloak’ requests and request them from libera.chat admins to process. (Cloaks are a hostmask with the project name in them instead of hostname or ip address. It’s a way to note your affiliation with a group)
* To (re)gain and configure control of irc channels in the group namespace “#fedora*”


== How to join the team / step down


Group contacts are added from very trusted project members and kept to a small number. Council should approve changes in libera.chat group members. Changes will then be made by libera admins to joining / leaving accounts.

== Responsibilities

* Process the ‘cloaks’ requests: https://fedoraproject.org/wiki/LiberaCloaks as time permits.
* Process requests in the https://pagure.io/irc tracker for matrix bridges or regaining control of a channel in the #fedora* namespace as requested.
* Idle in the #libera-communities channel to make requests to libera admins as needed.

== Teams you will be working closely with

* libera.chat admins in #libera.communities
* Any fedora groups needing irc work

== Current members

** Tom Calloway / spot
** Nick Bebout / nb
** Kevin Fenzi / nirik

== Contact information

Link : https://pagure.io/irc
