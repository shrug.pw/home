include::ROOT:partial$attributes.adoc[]

== Ambassadors Logos

Find all the CommOps logos & variations https://pagure.io/fedora-commops/blob/main/f/docs/modules/all-assets/Team%20Logos[here]. Repo structure is quite simple to navigate.

image::Ambassadors.png[]

