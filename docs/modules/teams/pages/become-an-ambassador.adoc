= Become an Ambassador
	
 
	
== Introduction
	
 
	
Do you think you might want to be a Fedora Ambassador? 

Here is some of what you can expect:
	
 
	
* You give a face and a name to Fedora. You are a person, friendly and approachable. 
Fedora can be big and scary, but you are going to help make sure that _one person_ finds their way through all the confusion into the place where they can contribute.
	
* You are the glue. You help connect different people in different parts of the Project because you are always dealing with people whose interests are different. You should be eager and happy to work with other parts of Fedora to solve problems.
	
* People are the key. Everything that we do -- events, budgets, swag, membership verifications, blogging -- it's all done as an attempt to make personal connections with folks who either want to use or contribute to Fedora. Places, where Ambassadors get together, are one of the few times that Fedora contributors have an opportunity to meet face to face (other than FUDCons and FADs).
	
* You are the expert in your region. You understand the culture, the needs, and the best ways to communicate about Fedora and FOSS in your region. Mindshare and other "leaders" in Fedora _work for you_, and need to listen to the needs of the local Fedora communities. We can't ignore the global picture, but we need to be flexible in adapting to local needs.
		
== Get Started with Fedora Ambassadors
	
 
	
To become a official Fedora Ambassador, there are a few steps you need to complete.
	
 
	
=== Step 1: Create your user wiki page
	
[IMPORTANT] 

.Requires CLA+1 status to complete
====
To edit the wiki, you now have to have CLA+1 status (which means you belong to at least one other group in FAS than the CLA-related groups).If you are unable to edit the wiki, skip this step and work with your mentor on a temporary workaround. Once
you have CLA+1 status, make sure to create a user wiki page. To learn
more about the Fedora CLA, visit this https://fedoraproject.org/wiki/Legal:Licenses/CLA[page].
==== 	
 
	
Creating your user page lets you introduce yourself to the Fedora
community. These pages let you collect useful information, like your
email, your real name, or your IRC nick, in a public place so that
others may contact you. Your user page is your own space, so feel free
to make it look as you wish. Here's what you'll need to do to create
	
Your user page:
	
 
	
* Go to https://fedoraproject.org/wiki/Special:MyPage[your user page].
	
**  Click create, at the top, to start editing your user page. If you've
	already created a user page, click edit.

** Write your introduction in the textbox. Your introduction can contain
whatever you'd like, butplease have an English translation. 

** When you're finished writing,click Save page.
	
 
	
Here's some ideas on what you can include in your introduction:
	
 
	
* Email address
	
* Homepage or blog
	
* Fedora Badges
	
* IRC channels you're typically in
	
* Pictures, headings*
	
* What you do, or would like to do, in the Fedora Project
	
[TIP]
====	
While a wiki user page can be created with just plain text, you'll
likely want to add some headings, bold text, or a picture. These can all
be added using markup. Every page on this wiki is made of this markup,
so you can use any of them as an example. Learn more about wiki syntax
and markup https://fedoraproject.org/wiki/Help:Wikisyntaxandmarkup[here]
====
	
 
	
If you're interested in examples, this user page template provides a
basic layout that you can copy. You can also look at the markup for
other users' pages. Here's a selection of some that have stood out:
	
 
	
* https://fedoraproject.org/wiki/User:Bcotton[Ben Cotton]
	
* https://fedoraproject.org/wiki/User:Siddharthvipul1[Vipul Siddharth]
	
* https://fedoraproject.org/wiki/User:Duffy[Máirín Duffy]
	
* https://fedoraproject.org/wiki/User:Jonatoni[Jona Azizaj]
	
 
	
=== Step 2: Explore the Fedora Project
	
As an Ambassador, you'll be asked about numerous parts of the Fedora
Project. You don't need to have all the answers, but you need general
familiarity with Fedora Project, and you'll need to know how to find answers.
You're most likely well versed in your area of interest, but there is a
lot happening in Fedora.
	
 
	
* Login to and explore
	https://discussion.fedoraproject.org/[discussion.fedoraproject.org].
	Follow the #ambassor & #mindshare tags
	
* Read the
	https://docs.fedoraproject.org/en-US/commops/contribute/successful-contributor/[how
	to be a successful contributor] page.
	
* Read https://fedoramagazine.org/[Fedora Magazine] and the
	https://communityblog.fedoraproject.org/[Community Blog] regularly.
	
Think about which parts of the Fedora Project capture your attention and
imagination.
	
* Read the http://fedoraplanet.org/[Planet Fedora] regularly.
	
* Read https://opensource.com/[opensource.com] for more open source news.
	
* Peruse the https://lists.fedoraproject.org/archives/[Fedora Mailing lists] as a lot of communication in Fedora happens on mailing lists.
	
** Read the https://fedoraproject.org/wiki/Mailing_list_guidelines[mailing list guidelines].
	
** Here are possible
	https://fedoraproject.org/wiki/Communicating_and_getting_help?rd=Communicate#Contributors_Mailing_Lists[lists
	to join].
	
** You will be asked to join the ambassadors list later on in this process.
	
* Join and observe in a variety of the Fedora chat channels from your chat platform of choice.
	
** Element/matrix is the recommended chat platform, learn more https://element.io/[here].
	
** #fedora-ambassadors is recommended, but a full list can be found https://fedoraproject.org/wiki/Communicating_and_getting_help?rd=Communicate#IRC[here]
	
 
	
=== Step 3: Subscribe to the Ambassadors Mailing List
	
 
	
* Subscribe to https://lists.fedoraproject.org/admin/lists/ambassadors.lists.fedoraproject.org/[Fedora Ambassadors Mailing List].
	
 
[NOTE]

====
There are a lot of people on the list, so make sure your mail is
relevant to the broader Ambassador audience. Please keep the discussion
focused on Fedora Ambassadors and save any introductions for chat
channels. This includes invites to social-networking sites, technical
support questions or advice, or anything else which is not Fedora
ambassador related.
====
	
 
	
=== Step 4: Find a mentor
	
 
	
A Mentor is your partner to get sponsored!
	
 
	
Mentors are experienced Ambassadors who can help you quickly become
acclimated to the work expected of Ambassadors as well as ensuring that
Ambassadors within Fedora do an acceptable job of representing Fedora.
	
 
	
[NOTE]
====
* Most mentors have full time jobs, families, and many responsibilities
within Fedora. Please be patient and do not expect an immediate reply.
	
* Mentors do not initiate contact, please contact a mentor to see if
they can support your Ambassadorship.
	
* Please note that even if you contact a mentor, they may already have
enough current obligations that they are committed to. In that case,
they may decline, and you should try to find a different mentor.
====
	
 
	
Finding a Mentor:

* Join our Ambassador Monthly Calls.
* If you already have potential mentor. Please contact the Mentor via email and introduce yourself. This email will help the Mentor understand your interest in Fedora Ambassador. 
* You can also talk to them at a conference or virtual event to establish a rappot, which can help you break the ice



Please include the following information:
* Your main purpose for joining the Fedora Ambassadors.
	
** Note: The mentor may ask you to expand upon this if it's incomplete, or seems inconsistent with the goals of the Fedora Ambassadors.
	
* A link to your Fedora User wiki page
	
* A bit of information about what you have done so far to promote Fedora amongst your peers/friends.
	
 
	
=== Step 5: Get approval to become an Ambassador
	
 
	
Confirm you completed the following steps to become a Fedora Ambassador:
	
 
	
. Create User Wiki Page
	
. Explore the Fedora Project
	
. Subscribe to the Ambassadors mailing list
	
. Find a mentor
	
 
	
Once you have verified the steps above, the approval process below will then begin. The approval process can take 60-90 days on average. 

If an Ambassador candidate is actively contributing to another Team within Fedora, their mentorship period may be shortened.

Approval process After you have completed all of the above steps, your mentor will approve and sponsor your membership to the Ambassador Group in FAS. Once you're official, https://discussion.fedoraproject.org/t/welcome-new-ambassadors/39224[comment] on the "Welcome New Ambassadors"
thread on Discussion.fedoraproject.org.
