include::ROOT:partial$attributes.adoc[]

= Overview

Various surveys were developed during the Community Outreach Revamp. The most useful questions were refined and collected here for future implementation. Any contributor interested in community metrics should feel free to run these polls using http://discussion.fedoraproject.org/[discussion.fedoraproject.org]. Record your findings on the Informal Poll Results wiki page.

Folks interested in community metrics and health should feel free to take the results and analyze the data. Please share your findings with a post on https://discussion.fedoraproject.org/c/project/commops[Discussion.fedoraproject.org] and use #commops as the topic.
