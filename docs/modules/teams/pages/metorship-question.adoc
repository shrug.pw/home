include::ROOT:partial$attributes.adoc[]

= Questionnaire for Potential Ambassador 

==== Please make a copy of this document. This is your “working document” - please share that copy back with your mentor with comments enabled

If you are reading this document, you have at some point expressed the desire to take on the formal mantle of a “Fedora Ambassador”. This document assumes that you

* Are already familiar with the relevant sections of the Ambassador docs
* Have attended a mentorship call and have a plan in place for sponsorship(CLA +1)
* Have been contributing to an area of Fedora (or more!)
* Are subscribed to the mailing list for ambassadors, mindshare and other areas of interest.
* Participate in conversations on the IRC channels

Remember, there are no right or wrong answers. This is the basis of a conversation.

* Tell us a bit about yourself (link to your Fedora user page) and how you became a part of The Fedora Project
* What have you been recently working on? Tell us a bit about your focus and goals (links to blog posts, streams/videos, discussions on list etc would be nice to have)
* What are your goals as a participant in The Fedora Project?
* How are you measuring if you are making progress towards your goals?
* What is going well with your goals and plans?
* What is not going so well with your goals and plans? How can the project help?
* Anything else you’d like to share ...

