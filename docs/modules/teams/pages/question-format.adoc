include::ROOT:partial$attributes.adoc[]

= Template for Question
This document outlines the types of question the organizer can run. The questions listed here are formatted for use on Discussion.fedoraproject.org.

== Single Choice
-------------------------------------------------
Question: Lorem ipsum
[poll type=regular results=always chartType=bar]

    Option 1
    Option 2
    Option 3
    [/poll]
-------------------------------------------------
== Multiple Choice

-------------------------------------------------------------------------
Question: Lorem ipsum
[poll name=poll2 type=multiple results=always min=1 max=2 chartType=bar]

    option 1
    or option 2
    or option 3
    [/poll]
-------------------------------------------------------------------------

= Schedule

These questions should be run as often as the CommOps and/or Mindshare team can initiate them.


